﻿using System.Linq;
using System.Web.Mvc;
using TelefonRehberi.Methods;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers
{
    public class PublicController : Controller
    {
        DatabaseEntities db = new DatabaseEntities();

        public ActionResult Index()
        {
            return View(db.tbl_Personel.ToList());
        }

        public ActionResult PersonelDetay(int id)
        {
            var model = new ViewModels.PersonelVM();

            try
            {
                var row = db.tbl_Personel.FirstOrDefault(x => x.Id == id);

                if (row != null)
                {
                    model.Firma = row.Firma;
                    model.Dahili = row.Dahili;
                    model.Email = row.Email;
                    model.Lokasyon = row.Lokasyon;
                    model.PersonelAdi = row.PersonelAdi;
                    model.PersonelSoyadi = row.PersonelSoyadi;
                    model.PersonelTelefon = row.PersonelTelefonNo;

                    var rowDepartman = db.tbl_Departman.FirstOrDefault(x => x.Id == row.DepartmanID);

                    if (rowDepartman != null)
                    {
                        model.Departman = rowDepartman.DepartmanAdi;
                    }

                    if (row.YoneticiID > 0)
                    {
                        var rowYonetici = db.tbl_Personel.FirstOrDefault(x => x.Id == row.YoneticiID);

                        if (rowYonetici != null)
                        {
                            model.Yonetici = string.Format("{0} {1}", rowYonetici.PersonelAdi, rowYonetici.PersonelSoyadi);
                        }
                    }

                }
            }
            catch (System.Exception ex)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View(model);
        }
    }
}