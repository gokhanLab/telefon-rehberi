﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using TelefonRehberi.Methods;
using TelefonRehberi.Models;
using TelefonRehberi.ViewModels;

namespace TelefonRehberi.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {

        DatabaseEntities db = new DatabaseEntities();

        CheckMethods _check = new CheckMethods();

        RehberViewMethods _rehberView = new RehberViewMethods();

        public ActionResult Index()
        {
            return View();
        }


        #region Departman
        public ActionResult DepartmanListesi()
        {
            var model = db.tbl_Departman.ToList();

            return View(model);
        }

        public ActionResult DepartmanEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DepartmanEkle(Departman departman)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _check.checkDepartman(departman);

                    return RedirectToAction("DepartmanListesi");
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return View();
            }
        }

        public ActionResult DepartmanGuncelle(int id)
        {
            var row = db.tbl_Departman.Where(x => x.Id == id);

            if (row != null)
            {
                return View(row.FirstOrDefault());
            }

            return View(new Departman());
        }

        [HttpPost]
        public ActionResult DepartmanGuncelle(Departman departman, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _check.updateDepartman(departman, id);

                    return RedirectToAction("DepartmanListesi");
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View();
            }

        }


        public ActionResult DepartmanSil(int id)
        {
            var row = db.tbl_Departman.Where(x => x.Id == id);

            if (row != null)
            {
                return View(row.FirstOrDefault());
            }

            return View(new Departman());
        }

        [HttpPost]
        public ActionResult DepartmanSil(Departman departman, int id)
        {
            try
            {
                _check.checkCalisanDepartman(id);

                return RedirectToAction("DepartmanListesi");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return View();
            }


        }

        public ActionResult DepartmanDetay(int id)
        {
            var row = db.tbl_Departman.Where(x => x.Id == id);

            if (row != null)
            {
                return View(row.FirstOrDefault());
            }

            return View(new Departman());
        }

        #endregion

        #region Personel

        public ActionResult PersonelEkle()
        {
            ViewBag.Departmanlar = db.tbl_Departman.ToList();
            ViewBag.Personeller = _rehberView.personelTamAd();

            return View();
        }

        [HttpPost]
        public ActionResult PersonelEkle(Personel personel)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (personel.Id == 0)
                    {
                        db.tbl_Personel.Add(personel);
                        db.SaveChanges();

                        return RedirectToAction("PersonelListele");
                    }
                    else
                    {
                        ViewBag.Message = "Var Olan Personeli Yeniden Ekleyemezsiniz";
                    }
                }

                ViewBag.Departmanlar = db.tbl_Departman.ToList();
                ViewBag.Personeller = _rehberView.personelTamAd();

                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return View();
            }
        }

        public ActionResult PersonelListele()
        {

            return View(db.tbl_Personel.ToList());
        }

        public ActionResult PersonelDetay(int id)
        {
            return View(_rehberView.personelView(id));
        }

        public ActionResult PersonelGuncelle(int id)
        {
            ViewBag.Departmanlar = db.tbl_Departman.ToList();
            ViewBag.Personeller = _rehberView.personelTamAd();

            return View(db.tbl_Personel.Find(id));
        }

        [HttpPost]
        public ActionResult PersonelGuncelle(Personel personel, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var row = personel.Id == id ? db.tbl_Personel.FirstOrDefault(x => x.Id == id) : null;

                    if (row != null)
                    {

                        row.PersonelAdi = personel.PersonelAdi;
                        row.PersonelDepartman = personel.PersonelDepartman;
                        row.PersonelSoyadi = personel.PersonelSoyadi;
                        row.PersonelTelefonNo = personel.PersonelTelefonNo;
                        row.DepartmanID = personel.DepartmanID;
                        row.Dahili = personel.Dahili;
                        row.YoneticiID = personel.YoneticiID;
                        row.Email = personel.Email;
                        row.Lokasyon = personel.Lokasyon;
                        row.Firma = personel.Firma;


                        db.Entry(row).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();


                        return RedirectToAction("PersonelDetay", new RouteValueDictionary(new { controller = "Admin", action = "PersonelDetay", Id = id }));

                    }

                }
                ViewBag.Departmanlar = db.tbl_Departman.ToList();
                ViewBag.Personeller = _rehberView.personelTamAd();
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return View();
            }

        }

        public ActionResult PersonelSil(int id)
        {
            return View(_rehberView.personelView(id));
        }

        [HttpPost]
        public ActionResult PersonelSil(PersonelVM personel, int id)
        {
            try
            {
                _check.checkPersonelYonetici(id);
                return RedirectToAction("PersonelListele");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return View();
            }

        }

        #endregion

        public ActionResult SifreDegistir()
        {
            return View();

        }
        [HttpPost]
        public ActionResult SifreDegistir(LoginVM newLogin)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (newLogin.Password == newLogin.ConfirmPassword)
                    {
                        var row = db.tbl_Login.FirstOrDefault(x => x.UserName.Equals(User.Identity.Name));

                        if (row != null)
                        {
                            row.Password = newLogin.Password;


                            db.Entry(row).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();


                            FormsAuthentication.SignOut();
                            Session.Abandon();
                            return RedirectToAction("Index", "Login");
                        }
                        else
                        {
                            throw new Exception("Geçerli Bir Kullanıcı Bulunamadı!");
                        }
                    }
                    else
                    {
                        throw new Exception("Şifreler Eşleşmiyor...");
                    }
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return View();
            }
        }


        [AllowAnonymous]
        public JsonResult getPersonelList()
        {
            var personelList = from p in db.tbl_Personel
                               join d in db.tbl_Departman
                               on p.DepartmanID equals d.Id
                               select new
                               {
                                   p.PersonelAdi,
                                   p.PersonelSoyadi,
                                   p.PersonelTelefonNo,
                                   p.Id,
                                   d.DepartmanAdi,
                                   p.Dahili,
                                   p.Email,
                                   p.Lokasyon
                               };

            return Json(personelList, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult getDepartmanList()
        {
            return Json(db.tbl_Departman.ToList(), JsonRequestBehavior.AllowGet);
        }

    }
}