﻿using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using TelefonRehberi.Models;
using TelefonRehberi.Methods;
using System.Linq;
using System.Web.Security;
using System;

namespace TelefonRehberi.Controllers
{
    public class LoginController : Controller
    {
        DatabaseEntities db = new DatabaseEntities();

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Public");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(Login data)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    var row = db.tbl_Login;

                    if (row.Count() == 0)
                    {

                        db.tbl_Login.Add(new Login() { UserName = "admin", Password = "admin" });
                        db.SaveChanges();


                        FormsAuthentication.SetAuthCookie(data.UserName, false);

                        return RedirectToAction("PersonelListele", "Admin");
                    }
                    else
                    {

                        var admin = row.FirstOrDefault(x => x.UserName == data.UserName && x.Password == data.Password);

                        if (admin == null)
                        {
                            throw new Exception("Bilgilerinizden en az birini hatalı girdiniz...");
                        }
                        else
                        {
                            FormsAuthentication.SetAuthCookie(data.UserName, false);

                            return RedirectToAction("PersonelListele", "Admin");
                        }

                    }
                }

                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return View();
            }
        }

        public ActionResult Cikis()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Login");
        }
    }
}
