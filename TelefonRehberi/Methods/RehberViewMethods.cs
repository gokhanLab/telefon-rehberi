﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelefonRehberi.Models;
using TelefonRehberi.ViewModels;

namespace TelefonRehberi.Methods
{
    public class RehberViewMethods
    {
        DatabaseEntities db = new DatabaseEntities();

        public PersonelVM personelView(int id)
        {
            var personel = db.tbl_Personel.Find(id);

            if (personel != null)
            {
                var personelVM = new PersonelVM
                {
                    PersonelID = personel.Id,
                    PersonelAdi = personel.PersonelAdi,
                    PersonelSoyadi = personel.PersonelSoyadi,
                    PersonelTelefon = personel.PersonelTelefonNo,
                    Dahili = personel.Dahili,
                    Email = personel.Email,
                    Lokasyon = personel.Lokasyon,
                    Firma = personel.Firma
                };

                var rowDepartman = db.tbl_Departman.FirstOrDefault(x => x.Id == personel.DepartmanID);

                if (rowDepartman != null)
                {
                    personelVM.Departman = rowDepartman.DepartmanAdi;
                }

                if (personel.YoneticiID > 0)
                {
                    var rowYonetici = db.tbl_Personel.FirstOrDefault(x => x.Id == personel.YoneticiID);

                    if (rowYonetici != null)
                    {
                        personelVM.Yonetici = string.Format("{0} {1}", rowYonetici.PersonelAdi, rowYonetici.PersonelSoyadi);
                    }
                }

                return personelVM;
            }

            return null;
        }

        public Dictionary<int, string> personelTamAd()
        {
            var personelList = db.tbl_Personel.ToList();

            var tamAdList = new Dictionary<int, string>
            {
                { 0, "Yönetici Seçiniz" }
            };

            foreach (var personel in personelList)
            {
                tamAdList.Add(personel.Id, string.Format("{0} {1}", personel.PersonelAdi, personel.PersonelSoyadi));
            }

            return tamAdList;

        } //Yönetici Dropdown Listesi için Dictionary döndüren metot

    }
}