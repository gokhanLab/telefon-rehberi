﻿using System;
using System.Linq;
using TelefonRehberi.Models;

namespace TelefonRehberi.Methods
{
    public class CheckMethods
    {
        DatabaseEntities db = new DatabaseEntities();

        public void checkDepartman(Departman departman)
        {
            var row = db.tbl_Departman.FirstOrDefault(x => x.DepartmanAdi.Equals(departman.DepartmanAdi));

            if (row == null)
            {
                db.tbl_Departman.Add(departman);
                db.SaveChanges();
            }
            else
            {
                throw new Exception("Departman Sistemde Mevcut");
            }

        }

        public void updateDepartman(Departman departman, int id)
        {

            var row = departman.Id == id ? db.tbl_Departman.FirstOrDefault(x => x.Id == id) : null;

            if (row == null)
            {
                throw new Exception("Güncellenecek Departman Bulunamadı!");
            }
            else
            {
                row.DepartmanAdi = departman.DepartmanAdi;

                db.Entry(row).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

            }

        }



        public int checkYonetici(Personel personel)
        {
            if (personel.YoneticiID != 0)
            {
                return personel.YoneticiID;
            }
            else
            {
                return 0;
            }
        }


        public void checkPersonelYonetici(int id)
        {
            var row = db.tbl_Personel.FirstOrDefault(x => x.YoneticiID == id);

            if (row == null)
            {
                var rowPersonel = db.tbl_Personel.FirstOrDefault(x => x.Id == id);

                if (rowPersonel != null)
                {
                    db.tbl_Personel.Remove(rowPersonel);
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Silenecek Personel Bulunamadı");
                }
            }
            else
            {
                throw new Exception("Bu personel en az bir personelin yöneticisidir, silme işlemi gerçekleşmedi...");
            }
        }

        public void checkCalisanDepartman(int id)
        {
            var row = db.tbl_Personel.FirstOrDefault(x => x.DepartmanID == id);

            if (row == null)
            {
                var rowDepartman = db.tbl_Departman.FirstOrDefault(x => x.Id == id);

                if (rowDepartman != null)
                {
                    db.tbl_Departman.Remove(rowDepartman);
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Silinecek Departman Bulunamadı!");
                }
            }
            else
            {
                throw new Exception("Bu departmanda çalışan personel bulunmaktadır, silme işlemi gerçekleşmedi...");
            }
        }
    }
}