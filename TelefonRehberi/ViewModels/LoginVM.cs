﻿using System.ComponentModel.DataAnnotations;

namespace TelefonRehberi.ViewModels
{
    public class LoginVM
    {

        private string _Password;
        [Required, DataType(DataType.Password)]
        public string Password
        {
            get { return Works.Formats.Sanitize(_Password); }
            set { _Password = value; }
        }

        private string _ConfirmPassword;
        [Required, DataType(DataType.Password)]
        public string ConfirmPassword
        {
            get { return Works.Formats.Sanitize(_ConfirmPassword); }
            set { _ConfirmPassword = value; }
        }

    }
}