﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    [Table("Personel")]
    public class Personel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        private string _PersonelAdi;
        [Required, MaxLength(45)]
        public string PersonelAdi
        {
            get { return Works.Formats.toUpper(Works.Formats.Sanitize(_PersonelAdi)); }
            set { _PersonelAdi = value; }
        }

        private string _PersonelSoyadi;
        [Required, MaxLength(45)]
        public string PersonelSoyadi
        {
            get { return Works.Formats.toUpper(Works.Formats.Sanitize(_PersonelSoyadi)); }
            set { _PersonelSoyadi = value; }
        }

        private string _PersonelTelefonNo;
        [Required, MaxLength(20)]
        public string PersonelTelefonNo
        {
            get { return Works.Formats.toPhone(_PersonelTelefonNo); }
            set { _PersonelTelefonNo = value; }
        }

        private string _Dahili;
        [MaxLength(10)]
        public string Dahili
        {
            get { return Works.Formats.Sanitize(_Dahili); }
            set { _Dahili = value; }
        }

        private string _Email;
        [MaxLength(55)]
        public string Email
        {
            get { return Works.Formats.toEmail(_Email); }
            set { _Email = value; }
        }

        private string _Lokasyon;
        [MaxLength(125)]
        public string Lokasyon
        {
            get { return Works.Formats.toUpper(Works.Formats.Sanitize(_Lokasyon)); }
            set { _Lokasyon = value; }
        }

        private string _Firma;
        [MaxLength(125)]
        public string Firma
        {
            get { return Works.Formats.toUpper(Works.Formats.Sanitize(_Firma)); }
            set { _Firma = value; }
        }


        public int YoneticiID { get; set; }

        [Required]
        public int DepartmanID { get; set; }

        public Departman PersonelDepartman { get; set; }

    }
}