﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TelefonRehberi.Models
{
    [Table("Login")]
    public class Login
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        private string _UserName;
        [Required, MaxLength(45), DataType(DataType.Text)]
        public string UserName
        {
            get { return Works.Formats.Sanitize(_UserName); }
            set { _UserName = value; }
        }

        private string _Password;
        [Required, DataType(DataType.Password), MaxLength(55)]
        public string Password
        {
            get { return Works.Formats.GetMd5Hash(Works.Formats.Sanitize(_Password)); }
            set { _Password = value; }
        }
    }
}