namespace TelefonRehberi.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class DatabaseEntities : DbContext
    {

        public DatabaseEntities() : base("name=DatabaseEntities")
        {

        }


        public virtual DbSet<Personel> tbl_Personel { get; set; }
        public virtual DbSet<Departman> tbl_Departman { get; set; }
        public virtual DbSet<Login> tbl_Login { get; set; }
    }


}