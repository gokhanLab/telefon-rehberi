﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TelefonRehberi.Models
{
    [Table("Departman")]
    public class Departman
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        private string _DepartmanAdi;
        [Required, MaxLength(255)]
        public string DepartmanAdi
        {
            get { return Works.Formats.toUpper(Works.Formats.Sanitize(_DepartmanAdi)); }
            set { _DepartmanAdi = value; }
        }

        public List<Personel> PersonelList { get; set; }
    }
}