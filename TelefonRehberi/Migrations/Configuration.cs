namespace TelefonRehberi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TelefonRehberi.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DatabaseEntities context)
        {
            base.Seed(context);
        }
    }
}
