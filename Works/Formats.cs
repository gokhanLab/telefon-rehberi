﻿using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Works
{
    public static class Formats
    {
        public static string toPhone(string value)
        {
            try
            {
                value = Sanitize(value);

                if (!string.IsNullOrEmpty(value))
                {
                    string[] ch = new string[] { "(", ")", "-", " " };
                    foreach (string chi in ch)
                    {
                        value = value.Replace(chi, "");
                    }
                    value = value.Length == 10 ? value.PadLeft(11, '0') : value.Length == 9 ? value.PadRight(11, '0') : value.Length < 9 ? string.Empty : value;
                }
                return value;
            }
            catch
            { }
            return string.Empty;
        }

        public static string toUpper(string value)
        {
            try
            {
                return value.ToUpper();
            }
            catch { }
            return string.Empty;
        }
        public static string Replace(object value, string replace, string result)
        {
            try
            {
                return value.ToString().Replace(replace, result);
            }
            catch
            { }
            return string.Empty;
        }

        public static string toUTF8(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    value = Regex.Replace(value, "Ö", "O");
                    value = Regex.Replace(value, "Ş", "S");
                    value = Regex.Replace(value, "İ", "I");
                    value = Regex.Replace(value, "Ü", "U");
                    value = Regex.Replace(value, "Ğ", "G");
                    value = Regex.Replace(value, "Ç", "C");
                    return value;
                }
            }
            catch
            { }
            return string.Empty;
        }
        public static string toEmail(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                    Encoding utf8 = Encoding.UTF8;
                    byte[] utfBytes = utf8.GetBytes(value.ToLower());
                    byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
                    return iso.GetString(isoBytes);
                }
            }
            catch
            { }
            return string.Empty;
        }
        public static string SaveSelect(string value)
        {
            try
            {
                value = Sanitize(value);

                if (!string.IsNullOrEmpty(value) && value != "-1" && !value.ToLower().Contains("seç"))
                {
                    return value;
                }
            }
            catch
            { }
            return string.Empty;
        }
        private static string RegexReplace(this string stringValue, string matchPattern, string toReplaceWith)
        {
            try
            {
                return Regex.Replace(stringValue, matchPattern, toReplaceWith);
            }
            catch
            { }
            return string.Empty;
        }
        private static string RegexReplace(this string stringValue, string matchPattern, string toReplaceWith, RegexOptions regexOptions)
        {
            try
            {
                return Regex.Replace(stringValue, matchPattern, toReplaceWith, regexOptions);
            }
            catch
            { }
            return string.Empty;
        }
        public static string Sanitize(this string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                    return string.Empty;

                return Sanitizer.GetSafeHtmlFragment(value.RegexReplace("-{2,}", "-")
                       .RegexReplace(@"[*/]+", string.Empty)
                       .RegexReplace(@"(<*)+(>*)", string.Empty)
                       .RegexReplace(@"(;|\s)(exec|execute|select|insert|update|delete|create|alter|drop|rename|truncate|backup|restore)\s", string.Empty, RegexOptions.IgnoreCase));
            }
            catch
            { }
            return string.Empty;
        }
        public static string Base64Decode(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var Bytes = System.Convert.FromBase64String(value);
                    return System.Text.Encoding.UTF8.GetString(Bytes);
                }
            }
            catch
            { }
            return string.Empty;
        }
        public static string Base64Encode(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var Bytes = System.Text.Encoding.UTF8.GetBytes(value);
                    return System.Convert.ToBase64String(Bytes);
                }
            }
            catch
            { }
            return string.Empty;
        }
        public static string GetVisitorIPAddress(bool GetLan = false)
        {
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (String.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            if (GetLan)
            {
                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    string stringHostName = Dns.GetHostName();

                    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                    IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                    try
                    {
                        visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                    }
                    catch
                    {
                        try
                        {
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            try
                            {
                                arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                visitorIPAddress = arrIpAddress[0].ToString();
                            }
                            catch
                            {
                                visitorIPAddress = "127.0.0.1";
                            }
                        }
                    }
                }
            }
            return visitorIPAddress;
        }
        public static string toContentDate(DateTime? date)
        {
            try
            {
                if (date != null)
                {
                    return Convert.ToDateTime(date).ToLongDateString();
                }
            }
            catch
            { }
            return string.Empty;
        }

        public static string PhoneMask(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    return string.Format("{0}*****{1}", value.Substring(0, 2), value.Substring(7, 4));
                }
            }
            catch
            { }
            return string.Empty;
        }
        public static string StripTagsRegex(string source)
        {
            try
            {
                return Regex.Replace(HttpUtility.HtmlDecode(source), "<.*?>", string.Empty);
            }
            catch
            { }
            return source;
        }
        public static string toSansur(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    string ret = string.Empty;
                    var name = value.Split(' ');

                    for (int i = 0; i < name.Length; i++)
                    {
                        if (name[i].Length > 1)
                        {
                            ret += string.Format("{0}*** ", name[i].Substring(0, 1));
                        }
                    }

                    return ret;
                }
                catch
                { }
            }
            return string.Empty;
        }
        public static string GetMd5Hash(string input)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

            byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        public static void RemoveCookie(HttpRequestBase Request, string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value) && Request != null)
                {
                    if (Request.Cookies[value] != null)
                    {
                        Request.Cookies[value].Expires = DateTime.Now;
                    }
                }
            }
            catch
            { }
        }
        public static string toISO1(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                    Encoding utf8 = Encoding.UTF8;
                    byte[] utfBytes = utf8.GetBytes(value.ToUpper());
                    byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
                    return iso.GetString(isoBytes);
                }
            }
            catch
            { }
            return string.Empty;
        }
        public static string toPhoneSave(string value)
        {
            value = Sanitize(value);
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Trim();
                if (value.Substring(0, 2) == "90")
                {
                    value = value.Substring(2);
                }

                long pOut = 0;
                string[] ch = new string[] { "(", ")", "-", " ", "+9", "_", "+", "/" };
                for (int i = 0; i < ch.Length; i++)
                {
                    value = value.Replace(ch[i], string.Empty);
                }

                if (!string.IsNullOrEmpty(value) && long.TryParse(value, out pOut) && value.All(char.IsDigit))
                {
                    return string.Format("{0}", pOut);
                }
            }
            return string.Empty;
        }

        public static string StripHTML(string value)
        {
            return Regex.Replace(value, "<.*?>", String.Empty);
        }
    }

}
